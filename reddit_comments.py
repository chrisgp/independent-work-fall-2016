import praw
import pickle
from datetime import datetime
from collections import namedtuple

user_agent = 'Chris\' cool comment scraper test v1.0'
start_timestamp = 1356998400  # 1/1/2013

Submission = namedtuple("Submission", "submission_id, created, created_utc, title, body, author_name, subreddit_name")
Comment = namedtuple("Comment", "comment_id, submission_id, created, created_utc, body, author_name, subreddit_name")


def submission_to_tuple(s):
    return Submission(s.id, s.created, s.created_utc, s.title, s.selftext, s.author.name, s.subreddit.title)


def comment_to_tuple(c):
    return Comment(c.id, c.submission.id, c.created, c.created_utc, c.body, c.author.name, c.subreddit.title)


def is_removed(item):
    """
    Checks if a comment or submission has been removed (the author is not accessible for removed items)
    :param item: comment or submission
    :return:
    """
    try:
        author = str(item.author.name)
    except:
        author = '[Deleted]'
    if author is '[Deleted]':
        return True
    else:
        return False


def _all_comments_helper(subreddit_name):
    r = praw.Reddit(user_agent)
    submissions = praw.helpers.submissions_between(r, subreddit_name, lowest_timestamp=start_timestamp)

    comments_ls = []
    count = 0
    for s in submissions:
        if count % 1000 == 0:
            print count, s.created, str(datetime.fromtimestamp(s.created))
        new_comments = praw.helpers.flatten_tree(s.comments)
        comments_ls += new_comments
        count += 1

    return comments_ls


def get_all_subreddit_comments(subreddit_name):
    # cache results
    filename = 'reddit_cache/' + subreddit_name + '.p'
    try:
        f = open(filename)
        flat_comments = pickle.load(f)
        f.close()
    except IOError:
        flat_comments = _all_comments_helper(subreddit_name)
        f = open(filename, 'w')
        pickle.dump(flat_comments, f)
        f.close()

    return flat_comments


def get_all_subreddit_submissions(subreddit_name):
    # cache results
    filename = 'reddit_cache/' + subreddit_name + '_submissions.p'
    try:
        f = open(filename)
        submissions = pickle.load(f)
        f.close()
    except IOError:
        r = praw.Reddit(user_agent)
        submissions = list(praw.helpers.submissions_between(r, subreddit_name, lowest_timestamp=start_timestamp))
        f = open(filename, 'w')
        pickle.dump(submissions, f)
        f.close()

    return submissions


def cache_all(subreddit_name, comments=False):
    print 'started', subreddit_name

    r = praw.Reddit(user_agent)
    submissions = praw.helpers.submissions_between(r, subreddit_name, lowest_timestamp=start_timestamp)  # 1480012530

    print submissions
    print 'received praw generator, now iterating'

    submissions_ls = []
    comments_ls = []
    count = 0
    for s in submissions:
        if not is_removed(s):
            submissions_ls.append(submission_to_tuple(s))
            if count % 10 == 0:
                print ('Number of submissions analyzed for comments: %d' % count), s.created, \
                    str(datetime.fromtimestamp(s.created))
            if comments:
                s.replace_more_comments(limit=None, threshold=0)
                new_comments = praw.helpers.flatten_tree(s.comments)
                new_comments = [comment_to_tuple(c) for c in new_comments if not is_removed(c)]
                comments_ls += new_comments
            count += 1

    print 'storing submissions'
    filename_s = 'reddit_cache/' + subreddit_name + '_submissions_tuple.p'
    f = open(filename_s, 'w')
    pickle.dump(submissions_ls, f)
    f.close()

    if comments:
        print 'storing comments'
        filename_c = 'reddit_cache/' + subreddit_name + '_comments_tuple.p'
        f = open(filename_c, 'w')
        pickle.dump(comments_ls, f)
        f.close()


def main():
    # # quick test
    # comments = get_all_subreddit_comments('monero')
    # for c in comments:
    #     print c.body[:20] + '@'*10

    # cache_all('bitcoin')
    cache_all('portland')


if __name__ == '__main__':
    main()
