import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from datetime import datetime
import numba
import pickle
import pytz
from reddit_comments import Submission
from tqdm import tqdm
from sklearn import linear_model, ensemble, tree


figure_size = (10, 4)

########################################################################################################################
# Get datasets from minimal input
########################################################################################################################


def get_trade_prices_and_timestamps(pair):
    f = open('good_data/' + pair + '_trades.csv')
    data = f.read().split('\n')
    f.close()
    trade_timestamps, prices = [], []
    for row in data:
        if len(row) < 2:
            continue
        cols = row.split(',')
        trade_timestamps.append(int(cols[0]))
        prices.append(float(cols[1]))

    return prices, trade_timestamps


def get_posts(subreddit):
    f = open('reddit_cache/' + subreddit + '_submissions_tuple.p')
    submissions = pickle.load(f)
    f.close()
    # post_timestamps = [int(s.created) for s in submissions]
    return submissions


########################################################################################################################
# Put data into nice pandas df format
########################################################################################################################


def _round_timestamp(t, period_length):
    return t - (t % period_length)


@numba.jit(nopython=True)
def _timestamp_frequencies_np(timestamps, first_timestamp, period_length, num_periods):
    """
    Makes a numpy array with no labeling, that can be combined with the time indices in posts_per_period to
    make a pd Dataframe with the right counts
    """
    counts = np.zeros(num_periods)
    for i in range(len(timestamps)):
        t = timestamps[i]
        index = (t - (t % period_length) - first_timestamp) / period_length
        counts[index] += 1
    return counts


@numba.jit(nopython=True)
def _trade_prices_np(timestamps, prices, first_timestamp, period_length, num_periods):
    """
    Makes a numpy array as in _timestamp_frequencies_np that represents a continuous time series, this time
    with prices. Assumes each timestamp corresponds to the price at the same index, and timestamps are sequential.
    """
    out = -1 * np.ones(num_periods)
    for i in range(len(timestamps)):
        t = timestamps[i]
        p = prices[i]
        index = (t - (t % period_length) - first_timestamp) / period_length
        out[index] = p
    for i in range(num_periods-1):
        if out[i+1] == -1.0:
            out[i+1] = out[i]
    return out


def timestamps_to_df(timestamps, period_length):
    """
    Converts a list of timestamps into an evenly-spaced pd DataFrame with counts of the number of timestamps per period
    :param timestamps:
    :param period_length: Length of each period in seconds
    :return: pd DataFrame of counts
    """
    start_dt = datetime.fromtimestamp(_round_timestamp(min(timestamps), period_length), tz=pytz.UTC)
    end_dt = datetime.fromtimestamp(_round_timestamp(max(timestamps), period_length), tz=pytz.UTC)
    datetime_indices = pd.date_range(start=start_dt, end=end_dt, freq=('%ds' % period_length))
    count_values = _timestamp_frequencies_np(np.array(timestamps), _round_timestamp(min(timestamps), period_length),
                                             period_length, len(datetime_indices))
    df = pd.DataFrame({'submissions': pd.Series(count_values.astype(np.double), index=datetime_indices)})

    return df


def prices_to_df(timestamps, prices, period_length):
    start_dt = datetime.fromtimestamp(_round_timestamp(min(timestamps), period_length), tz=pytz.UTC)
    end_dt = datetime.fromtimestamp(_round_timestamp(max(timestamps), period_length), tz=pytz.UTC)
    datetime_indices = pd.date_range(start=start_dt, end=end_dt, freq=('%ds' % period_length))
    structured_prices = _trade_prices_np(np.array(timestamps), np.array(prices),
                                         _round_timestamp(min(timestamps), period_length),
                                         period_length, len(datetime_indices))
    df = pd.DataFrame({'prices': pd.Series(structured_prices.astype(np.double), index=datetime_indices)})

    return df


def user_previous_activity(period_posts):
    """
    Computes the previous activity of each user who submits posts
    :param period_posts:
    :return:
    """

    # dictionary of all users as we progress through the list, allows for a single pass
    all_user_activity = {}

    # keep track of global stats to compute more independent stats
    total_posts, total_users = 0, 0

    # list of dictionaries corresponding to each period.
    # keys are users who post in the period and values are number of posts for that user preceding the period
    period_activity = []
    # quantile of previous activity, per-user
    period_quantiles = []

    # print 'computing users\' previous activity:'
    for period in tqdm(period_posts):
        users_in_period = [p.author_name for p in period]
        unique_users = list(set(users_in_period))

        # add new activity
        activity = {}  # previous activity of unqiue_users
        for user in unique_users:
            if user in all_user_activity:
                activity[user] = all_user_activity[user]
            else:
                activity[user] = 0
        period_activity.append(activity)

        # add new quantiles
        quantiles = {}  # previous activity of unqiue_users
        all_users_sorted = sorted(all_user_activity.values())
        for user in unique_users:
            if activity[user] == 0:
                quantiles[user] = 0.0
            else:
                quantiles[user] = all_users_sorted.index(activity[user]) * 1.0 / len(all_users_sorted)
        period_quantiles.append(quantiles)

        # update data structure
        for user in users_in_period:
            if user not in all_user_activity:
                all_user_activity[user] = 0
            all_user_activity[user] += 1

    return period_activity, period_quantiles, all_user_activity


def make_full_df(subreddit, pair, period_length):
    # get posts
    posts = get_posts(subreddit)
    post_timestamps = [int(x.created_utc) for x in posts]
    posts_df = timestamps_to_df(post_timestamps, period_length)

    # get price data
    trade_prices, trade_timestamps = get_trade_prices_and_timestamps(pair)
    prices_df = prices_to_df(trade_timestamps, trade_prices, period_length)

    # lag and get returns
    returns = pd.Series(prices_df.diff().prices.values[1:], index=prices_df.index[:-1])
    lag_returns = pd.Series(prices_df.diff().prices.values[2:], index=prices_df.index[:-2])

    # combine into one df
    df = pd.DataFrame({'submissions': posts_df.submissions, 'prices': prices_df.prices,
                       'returns': returns, 'lag_returns': lag_returns})
    df = df.dropna()

    # add abs of returns
    df['abs_returns'] = pd.Series(df.returns.abs(), df.index)
    df['abs_lag_returns'] = pd.Series(df.lag_returns.abs(), df.index)

    # separate posts by time period
    epoch_times = df.index.astype(np.int64) / 1000000000  # converts to seconds since epoch
    rounded_post_times = np.array([int(p.created_utc)-int(p.created_utc) % period_length for p in posts])
    period_posts = []
    for period_time in tqdm(epoch_times):
        period_posts.append([posts[i] for i in np.where(rounded_post_times == period_time)[0]])

    period_user_activity, period_user_quantiles, activity_totals = user_previous_activity(period_posts)

    df['top_author_quantile'] = pd.Series(
        np.array([(max(q.values()) if len(q) > 0 else 0) for q in period_user_quantiles]), df.index)

    # word occurences
    def word_in_post_titles(word, post_list):
        if len(post_list) == 0:
            return 0
        return max([(1 if word.lower() in post.title.lower() else 0) for post in post_list])

    df['hack'] = pd.Series(np.array([word_in_post_titles('hack', p_ls) for p_ls in period_posts]), df.index)
    df['accept'] = pd.Series(np.array([word_in_post_titles('accept', p_ls) for p_ls in period_posts]), df.index)

    # return posts, period_posts, period_user_activity, period_user_quantiles, activity_totals, df
    return df


def make_full_df_mem(subreddit, pair, period_length):
    version = '01'
    filename = 'full_df_cache/' + '_'.join(map(str, [subreddit, pair, period_length, version])) + '.p'
    try:
        f = open(filename)
        df = pickle.load(f)
        f.close()
    except IOError:
        print 'Cache Miss for ' + filename + ', loading data...'
        df = make_full_df(subreddit, pair, period_length)
        f = open(filename, 'w')
        pickle.dump(df, f)
        f.close()

    return df


########################################################################################################################
# Do something with data acquired
########################################################################################################################

def plot_scatter_with_fit(data1, data2):
    plt.figure(figsize=figure_size)
    plt.scatter(data1, data2, alpha=.1)

    # get line of best fit
    # from: stackoverflow.com/questions/19068862/how-to-overplot-a-line-on-a-scatter-plot-in-python/19069001#19069001
    m, b = np.polyfit(data1, data2, 1)  # abs returns
    # m, b = np.polyfit(df.submissions.values, df.lag_returns.values, 1)  # normal returns
    r2 = r_squared(data2, m * data1 + b)
    print 'line of best fit: y = %f * x + %f' % (m, b)
    print 'R^2 = ', r2
    plt.plot(data1, m * data1 + b, 'r')


def plot_simple_comparison(subreddit, pair, period_length):
    df = make_full_df_mem(subreddit, pair, period_length)

    plt.figure(figsize=figure_size)
    ax1 = plt.subplot(211)
    ax1.set_ylabel('r/' + subreddit + ' submissions')
    # posts_df.submissions.plot() # should work, but kinda slow compared to the next line
    plt.plot(df.index, df.submissions)
    ax2 = plt.subplot(2, 1, 2, sharex=ax1)
    ax2.set_ylabel(pair + ' price')
    # prices_df.prices.plot() # should work, kinda slow compared to the next line
    plt.plot(df.index, df.prices)
    plt.show()


def plot_posts_vs_returns_scatter(subreddit, pair, period_length, axes=None):
    """
    Currently returns the scatter of abs of the returns over the number of submissions.
    :param subreddit:
    :param pair:
    :param period_length:
    :param axes:
    :return:
    """
    df = make_full_df_mem(subreddit, pair, period_length)

    if axes is None:
        plt.figure(figsize=figure_size)
        ax = plt.subplot(111)
    else:
        ax = axes
    plt.hold(True)
    ax.set_xlabel('number of submissions in a given %dmin period' % (period_length / 60))
    ax.set_ylabel('returns in the following %dmin period' % (period_length / 60))

    plt.scatter(df.submissions, df.lag_returns, alpha=.1)  # abs returns
    # plt.scatter(df.submissions, df.lag_returns, alpha=.1)  # normal returns

    # get line of best fit
    # from: stackoverflow.com/questions/19068862/how-to-overplot-a-line-on-a-scatter-plot-in-python/19069001#19069001
    m, b = np.polyfit(df.submissions.values, df.lag_returns.values, 1)  # abs returns
    # m, b = np.polyfit(df.submissions.values, df.lag_returns.values, 1)  # normal returns
    r2 = r_squared(df.lag_returns, m * df.submissions.values + b)
    ax.set_title('Returns over Submissions, R^2=%.5f' % r2)
    print 'line of best fit: y = %f * x + %f' % (m, b)
    print 'periods used: ', len(df.submissions)
    plt.plot(df.submissions.values, m * df.submissions.values + b, 'r')

    if axes is None:
        plt.show()

    return df


def plot_posts_vs_abs_returns_scatter(subreddit, pair, period_length, axes=None):
    """
    Currently returns the scatter of abs of the returns over the number of submissions.
    :param subreddit:
    :param pair:
    :param period_length:
    :param axes:
    :return:
    """
    df = make_full_df_mem(subreddit, pair, period_length)

    if axes is None:
        plt.figure(figsize=figure_size)
        ax = plt.subplot(111)
    else:
        ax = axes
    plt.hold(True)
    ax.set_xlabel('number of submissions in a given %dmin period' % (period_length / 60))
    ax.set_ylabel('abs(returns) in the following %dmin period' % (period_length / 60))

    plt.scatter(df.submissions, df.abs_lag_returns, alpha=.1)  # abs returns
    # plt.scatter(df.submissions, df.lag_returns, alpha=.1)  # normal returns

    # get line of best fit
    # from: stackoverflow.com/questions/19068862/how-to-overplot-a-line-on-a-scatter-plot-in-python/19069001#19069001
    m, b = np.polyfit(df.submissions.values, df.abs_lag_returns.values, 1)  # abs returns
    # m, b = np.polyfit(df.submissions.values, df.lag_returns.values, 1)  # normal returns
    r2 = r_squared(df.abs_lag_returns, m * df.submissions.values + b)
    ax.set_title('Abs(Returns) over Submissions, R^2=%.5f' % r2)
    print 'line of best fit: y = %f * x + %f' % (m, b)
    print 'periods used: ', len(df.submissions)
    plt.plot(df.submissions.values, m * df.submissions.values + b, 'r')

    if axes is None:
        plt.show()

    return df


def plot_posts_vs_abs_returns_changes_scatter(subreddit, pair, period_length, axes=None):
    """
    Currently returns the scatter of abs of the returns over the number of submissions.
    :param subreddit:
    :param pair:
    :param period_length:
    :param axes:
    :return:
    """
    df = make_full_df_mem(subreddit, pair, period_length)

    submissions = df.submissions.values[:-1]
    abs_return_changes = np.diff(df.abs_lag_returns.values)

    if axes is None:
        plt.figure(figsize=figure_size)
        ax = plt.subplot(111)
    else:
        ax = axes
    plt.hold(True)
    ax.set_xlabel('number of submissions in a given %dmin period' % (period_length / 60))
    ax.set_ylabel('change in abs(returns) in the following %dmin period' % (period_length / 60))

    plt.scatter(submissions, abs_return_changes, alpha=.1)  # abs returns
    # plt.scatter(df.submissions, df.lag_returns, alpha=.1)  # normal returns

    # get line of best fit
    # from: stackoverflow.com/questions/19068862/how-to-overplot-a-line-on-a-scatter-plot-in-python/19069001#19069001
    m, b = np.polyfit(submissions, abs_return_changes, 1)  # abs returns
    # m, b = np.polyfit(df.submissions.values, df.lag_returns.values, 1)  # normal returns
    r2 = r_squared(abs_return_changes, m * submissions + b)
    ax.set_title('Abs(Returns) over Submissions, R^2=%.5f' % r2)
    print 'line of best fit: y = %f * x + %f' % (m, b)
    print 'periods used: ', len(submissions)
    plt.plot(submissions, m * submissions + b, 'r')

    if axes is None:
        plt.show()

    return df


def plot_all_periods(subreddit, pair):
    periods = [1, 2, 3, 6, 12, 24]
    main_axes = None
    for i, t in enumerate(periods):
        if main_axes is not None:
            ax = plt.subplot(2, 3, i+1, sharey=main_axes)
        else:
            ax = plt.subplot(2, 3, i + 1)
            main_axes = ax

        plot_posts_vs_abs_returns_scatter(subreddit, pair, t*3600, axes=ax)


def plot_feature(feature, feature_name, df, ax, period_length, fit_line=True):
    ax.set_xlabel(feature_name + ' in a given %dmin period' % (period_length / 60))
    ax.set_ylabel('abs(returns) in the following %dmin period' % (period_length / 60))

    plt.scatter(feature, df.abs_lag_returns, alpha=.1)  # abs returns
    # plt.scatter(feature, df.lag_returns, alpha=.1)  # normal returns

    if fit_line:
        # get line of best fit
        # from:stackoverflow.com/questions/19068862/how-to-overplot-a-line-on-a-scatter-plot-in-python/19069001#19069001
        #[np.where(feature.values > .985)[0]]
        m, b = np.polyfit(feature.values, df.abs_lag_returns.values, 1)  # abs returns
        # m, b = np.polyfit(feature.values, df.lag_returns.values, 1)  # normal returns
        print 'line of best fit: y = %f * x + %f' % (m, b)
        print 'periods used: ', len(feature)
        r2 = r_squared(df.abs_lag_returns.values, m * feature.values + b)
        ax.set_title('Abs(Returns) over Top Author Quantile, R^2=%.5f' % r2)
        plt.plot(feature.values, m * feature.values + b, 'r')


def plot_variables_against_returns(subreddit, pair, period_length):
    """
    Plots all features against lagged returns in one big plt.figure
    :param subreddit:
    :param pair:
    :param period_length:
    :return:
    """
    df = make_full_df_mem(subreddit, pair, period_length)

    features = [df.submissions, df.returns, df.abs_returns, df.top_author_quantile, df.hack, df.accept]
    feature_names = ['number of submissions', 'current period returns', 'current period abs(returns)',
                     'top author activity quantile', 'posts contain \'hack\'', 'posts contain \'accept\'']

    for i, feature, feature_name in zip(list(range(len(features))), features, feature_names):
        ax = plt.subplot(2, 3, i)
        plot_feature(feature, feature_name, df, ax, period_length)
    plt.show()

    return df


def plot_keywords_against_returns(subreddit, pair, period_length):
    """
    Plots all features against lagged returns in one big plt.figure
    :param subreddit:
    :param pair:
    :param period_length:
    :return:
    """
    df = make_full_df_mem(subreddit, pair, period_length)

    features = [df.hack, df.accept]
    feature_names = ['posts contain \'hack\'', 'posts contain \'accept\'']

    for i, feature, feature_name in zip(list(range(len(features))), features, feature_names):
        ax = plt.subplot(1, 2, i)
        plot_feature(feature, feature_name, df, ax, period_length, fit_line=False)
    plt.show()

    return df


def plot_user_max_quantile(subreddit, pair, period_length):
    """
    Plots all features against lagged returns in one big plt.figure
    :param subreddit:
    :param pair:
    :param period_length:
    :return:
    """
    df = make_full_df_mem(subreddit, pair, period_length)

    ax = plt.subplot(1, 1, 1)
    plot_feature(df.top_author_quantile, 'top author activity quantile', df, ax, period_length, fit_line=True)
    plt.show()

    return df

########################################################################################################################
# Main
########################################################################################################################


def r_squared(actual, estimates):
    ss_reg = sum(np.square(actual - estimates))
    mean = sum(actual) * 1.0 / len(actual)
    ss_tot = sum(np.square(actual - mean))

    return 1 - (ss_reg / ss_tot)


def full_ml(subreddit, pair, period_length=3*60*60, plot_sample_results=True, strategy=0):
    df = make_full_df_mem(subreddit, pair, period_length)

    df_train = df.ix['2012-01-01':'2016-05-31']
    df_test = df.ix['2016-06-01':'2017-01-01']

    x_train = np.transpose(np.array([#df_train.abs_returns.values,
                               df_train.top_author_quantile.values, df_train.submissions.values]))
    y_train = df_train.abs_lag_returns.values

    if strategy == 0:
        regressor = linear_model.LinearRegression()
    elif strategy == 1:
        regressor = tree.DecisionTreeRegressor(max_depth=4)
    elif strategy == 2:
        regressor = ensemble.RandomForestRegressor(n_estimators=100, max_depth=4)
    regressor.fit(x_train, y_train)

    # if strategy == 0:
    #     print 'Regression intercept: ', regressor.intercept_, ' and coefs: ', regressor.coef_

    y_train_preds = regressor.predict(x_train)

    # print 'sum of top author quantiles: ', sum(df_test.top_author_quantile)

    x_test = np.transpose(np.array([#df_test.abs_returns.values,
                                     df_test.top_author_quantile.values, df_test.submissions.values]))
    y_test = df_test.abs_lag_returns.values

    y_test_preds = regressor.predict(x_test)

    # print 'in sample r^2 = ', r_squared(y_train, y_train_preds)
    # print 'out of sample r^2 = ', r_squared(y_test, y_test_preds)

    if plot_sample_results:
        plt.figure()
        ax1 = plt.subplot(1, 2, 1)
        r2 = r_squared(y_train, y_train_preds)
        ax1.set_title('In-sample actual over predicted, R^2=%.5f' % (r2))
        ax1.set_xlabel('Predicted Values')
        ax1.set_ylabel('Actual Values')
        plt.scatter(y_train_preds, y_train)
        ax2 = plt.subplot(1, 2, 2)
        r2 = r_squared(y_test, y_test_preds)
        ax2.set_title('Out-of-sample actual over predicted, R^2=%.5f' % (r2))
        ax2.set_xlabel('Predicted Values')
        ax2.set_ylabel('Actual Values')
        plt.scatter(y_test_preds, y_test)
        plt.show()

        # plt.figure()
        # ax1 = plt.subplot(1, 2, 1)
        # train_actual_diffs = y_train[1:] - y_train[:-1]
        # train_predicted_diffs = y_train_preds[1:] - y_train[:-1]
        # r2 = r_squared(train_actual_diffs, train_predicted_diffs)
        # ax1.set_title('In-sample actual diff over predicted diff, R^2=%.5f' % (r2))
        # ax1.set_xlabel('Predicted Values')
        # ax1.set_ylabel('Actual Values')
        # plt.scatter(train_predicted_diffs, train_actual_diffs)
        # ax2 = plt.subplot(1, 2, 2)
        # test_actual_diffs = y_test[1:] - y_test[:-1]
        # test_predicted_diffs = y_test_preds[1:] - y_test[:-1]
        # r2 = r_squared(test_actual_diffs, test_predicted_diffs)
        # ax2.set_title('Out-of-sample actual diff over predicted diff, R^2=%.5f' % (r2))
        # ax2.set_xlabel('Predicted Values')
        # ax2.set_ylabel('Actual Values')
        # plt.scatter(test_predicted_diffs, test_actual_diffs)
        # plt.show()

    train_times = df_train.index.values
    test_times = df_test.index.values

    return regressor, y_train, y_train_preds, y_test, y_test_preds, train_times, test_times


def backtest(subreddit, pair, fees=0):
    plt.figure(figsize=figure_size)
    ax = plt.subplot(111)
    ax.set_title('Variance Trading Strategy Profit over Time')
    ax.set_xlabel('Time')
    ax.set_ylabel('Profit')

    strategy_names = ['Linear Regression', 'Decision Tree Regression', 'Random Forest Regression', 'Average-Based Prediction']
    colors = ['g', 'b', 'm', 'c']
    regressor_lines = []
    all_preds = []
    regressor_bankroll_results = []
    for strategy in range(4):
        # _, actual, preds, _, _, times, _ = full_ml(subreddit, pair, plot_sample_results=False, strategy=strategy)  # in sample
        if strategy < 3:
            _, in_sample_actual, in_sample_preds, actual, preds, _, times = full_ml(subreddit, pair, plot_sample_results=False, strategy=strategy)  # oo sample
        else:
            _, in_sample_actual, in_sample_preds, actual, preds, _, times = avg_predictions(subreddit, pair)  # oo sample

        actual_returns = np.diff(actual)

        # need to compare prediction for next period with actual current period
        # actual[:-1] is the abs return on period 1, 2, 3, etc going up to the second to last period, doesn't have first

        directions = ((preds[1:] > actual[:-1]) * 2 - 1)

        print
        print strategy_names[strategy], 'num up direction:', sum(directions == 1)
        print 'in sample R^2:', r_squared(np.diff(in_sample_actual), in_sample_preds[1:] - in_sample_actual[:-1])
        print 'out of sample R^2:', r_squared(np.diff(actual), preds[1:] - actual[:-1])
        print

        if strategy == 3:
            print 'avg stuff:'
            print in_sample_preds
            print sum(in_sample_actual) * 1.0 / len(in_sample_actual)

        regressor_bankroll = np.cumsum((actual[1:] - actual[:-1]) * directions)
        cum_fees = np.cumsum(actual[:len(regressor_bankroll)] * (-1.0) * fees)
        regressor_bankroll = regressor_bankroll + cum_fees

        regressor_bankroll_results.append(regressor_bankroll[-1])
        regressor_line, = plt.plot(times[1:], regressor_bankroll, c=colors[strategy], label=strategy_names[strategy])
        regressor_lines.append(regressor_line)

        all_preds.append(preds)

    # random choices for comparison
    random_lines = []
    for i in range(10):
        random_directions = np.random.random_integers(2, size=len(actual_returns))*2 - 3
        random_bankroll = np.cumsum(np.diff(actual) * random_directions)
        cum_fees = np.cumsum(actual[:len(random_bankroll)] * (-1.0) * fees)
        random_bankroll = random_bankroll + cum_fees

        line, = plt.plot(times[1:], random_bankroll, c='r', alpha=.4, label='Random choices')
        random_lines.append(line)

    plt.legend(handles=regressor_lines + [random_lines[0]], loc=2)
    # plt.legend(handles=regressor_lines, loc=2)
    plt.show()

    # monte carlo demo
    # random_choices_results = []
    # monte_carlo_size = 1000000
    # for _ in tqdm(range(monte_carlo_size)):
    #     random_directions = np.random.random_integers(2, size=len(actual_returns)) * 2 - 3
    #     random_result = np.sum(np.diff(actual) * random_directions)
    #
    #     random_choices_results.append(random_result)
    # monte_carlo_values = np.array(random_choices_results)
    #
    # print 'for ' + str(monte_carlo_size) + ' values:'
    # for p in [50, 75, 90, 95, 98, 99, 99.9, 99.99, 99.999, 99.9999, 99.99999]:
    #     print ('%.5fth percentile: %f' % (p, np.percentile(monte_carlo_values, p)))
    #
    # print '\n'

    for i in range(4):
        print strategy_names[i] + ' final balance: ' + str(regressor_bankroll_results[i])
    print '\nbest possible result:', np.sum(np.abs(np.diff(actual)))

    return all_preds, times

#
# def backtest_examination(subreddit, pair, fees=0):
#     plt.figure()
#     ax = plt.subplot(511)
#     ax.set_title('Variance Trading Strategy Profit over Time')
#     ax.set_xlabel('Time')
#     ax.set_ylabel('Profit')
#
#     strategy_names = ['Linear Regression', 'Decision Tree Regression', 'Random Forest Regression']
#     colors = ['g', 'b', 'm']
#     regressor_lines = []
#     all_preds = []
#     regressor_bankroll_results = []
#     for strategy in range(1):
#         # _, actual, preds, _, _, times, _ = full_ml(subreddit, pair, plot_sample_results=False, strategy=strategy)  # in sample
#         # _, _, _, actual, preds, _, times = full_ml(subreddit, pair, False, strategy=strategy)  # oo sample
#
#         _, _, _, actual, preds, _, times = avg_predictions(subreddit, pair)  # oo sample
#
#         actual_returns = np.diff(actual)
#
#         # need to compare prediction for next period with actual current period
#         # actual[:-1] is the abs return on period 1, 2, 3, etc going up to the second to last period, doesn't have first
#
#         directions = ((preds[1:] > actual[:-1]) * 2 - 1)
#
#         # regressor_bankroll = np.cumsum(np.diff(actual) * directions)
#         regressor_bankroll = np.cumsum((actual[1:] - actual[:-1]) * directions)
#
#         cum_fees = np.cumsum(actual[:len(regressor_bankroll)] * (-1.0) * fees)
#         regressor_bankroll = regressor_bankroll + cum_fees
#
#         regressor_bankroll_results.append(regressor_bankroll[-1])
#         regressor_line, = plt.plot(times[1:], regressor_bankroll, c=colors[strategy], label=strategy_names[strategy])
#         regressor_lines.append(regressor_line)
#
#         plt.subplot(5, 1, 2, sharex=ax)
#         plt.plot(times[1:], preds[1:])
#         plt.subplot(5, 1, 3, sharex=ax)
#         plt.plot(times[1:], actual[:-1])
#         ax4 = plt.subplot(5, 1, 4, sharex=ax)
#         ax4.set_ylim([-1.2, 1.2])
#         plt.plot(times[1:], directions)
#         plt.subplot(5, 1, 5, sharex=ax)
#         plt.plot(times[1:], (actual[1:] - actual[:-1]) * directions, marker='x')
#
#         all_preds.append(preds)
#
#     # # random choices for comparison
#     # random_lines = []
#     # for i in range(10):
#     #     random_directions = np.random.random_integers(2, size=len(actual_returns))*2 - 3
#     #     random_bankroll = np.cumsum(np.diff(actual) * random_directions)
#     #     cum_fees = np.cumsum(actual[:len(random_bankroll)] * (-1.0) * fees)
#     #     random_bankroll = random_bankroll + cum_fees
#     #
#     #     line, = plt.plot(times[:-1], random_bankroll, c='r', alpha=.4, label='Random choices')
#     #     random_lines.append(line)
#
#     plt.legend(handles=regressor_lines, loc=2)
#     plt.show()
#
#     # # monte carlo demo
#     # random_choices_results = []
#     # monte_carlo_size = 1000000
#     # for _ in tqdm(range(monte_carlo_size)):
#     #     random_directions = np.random.random_integers(2, size=len(actual_returns)) * 2 - 3
#     #     random_result = np.sum(np.diff(actual) * random_directions)
#     #
#     #     random_choices_results.append(random_result)
#     # monte_carlo_values = np.array(random_choices_results)
#     #
#     # print 'for ' + str(monte_carlo_size) + ' values:'
#     # for p in [50, 75, 90, 95, 98, 99, 99.9, 99.99, 99.999, 99.9999, 99.99999]:
#     #     print ('%.5fth percentile: %f' % (p, np.percentile(monte_carlo_values, p)))
#     #
#     print '\n'
#     for i in range(1):
#         print strategy_names[i] + ' final balance: ' + str(regressor_bankroll_results[i])
#     print '\nbest possible result:', np.sum(np.abs(np.diff(actual)))
#
#     return all_preds, times
#
#
# def counterexample_predictions(subreddit, pair):
#     df = make_full_df_mem(subreddit, pair, 3*60*60)
#
#     df_train = df.ix['2012-01-01':'2016-05-31']
#     df_test = df.ix['2016-06-01':'2017-01-01']
#
#     x_train = np.transpose(np.array([#df_train.abs_returns.values
#                                      df_train.index.values.astype(float)]))  #,
#                                # df_train.top_author_quantile.values, df_train.submissions.values]))
#     y_train = df_train.abs_lag_returns.values
#
#     regressor = linear_model.LinearRegression()
#     regressor.fit(x_train, y_train)
#
#     print 'Regression intercept: ', regressor.intercept_, ' and coefs: ', regressor.coef_
#
#     y_train_preds = regressor.predict(x_train)
#
#     print 'sum of top author quantiles: ', sum(df_test.top_author_quantile)
#
#     x_test = np.transpose(np.array([#df_test.abs_returns.values
#                                     df_test.index.values.astype(float)])) #,
#                                      # df_test.top_author_quantile.values, df_test.submissions.values]))
#     y_test = df_test.abs_lag_returns.values
#
#     y_test_preds = regressor.predict(x_test)
#
#     train_times = df_train.index.values
#     test_times = df_test.index.values
#
#     return regressor, y_train, y_train_preds, y_test, y_test_preds, train_times, test_times


def avg_predictions(subreddit, pair):
    df = make_full_df_mem(subreddit, pair, 3*60*60)

    df_train = df.ix['2012-01-01':'2016-05-31']
    df_test = df.ix['2016-06-01':'2017-01-01']

    y_train = df_train.abs_lag_returns.values

    y_train_avg = sum(y_train) * 1.0 / len(y_train)

    y_train_preds = np.ones(len(y_train)) * y_train_avg

    print 'avg_pred~ Regression intercept: ', y_train_avg
    print 'avg_pred~ In sample R^2 = ', r_squared(y_train, y_train_preds)

    y_test = df_test.abs_lag_returns.values

    y_test_preds = np.ones(len(y_test)) * y_train_avg

    train_times = df_train.index.values
    test_times = df_test.index.values

    return None, y_train, y_train_preds, y_test, y_test_preds, train_times, test_times


def fee_comparison(subreddit, pair):
    plt.figure()
    ax = plt.subplot(111)
    # ax.set_title('Variance Trading Strategy Profit over Time for Various Fees')
    ax.set_xlabel('Time')
    ax.set_ylabel('Profit')

    fees_ls = [0.0, 0.01, 0.05, 0.1, 0.5, 1.0]
    strategy_name = 'Linear Regression'
    regressor_lines = []
    for i, fee in enumerate(fees_ls):
        # _, actual, preds, _, _, times, _ = full_ml(subreddit, pair, plot_sample_results=False, strategy=strategy) # in sample
        _, _, _, actual, preds, _, times = full_ml(subreddit, pair, plot_sample_results=False, strategy=0)  # oo sample

        directions = ((preds[1:] > actual[:-1]) * 2 - 1)

        regressor_bankroll = np.cumsum(np.diff(actual) * directions)
        cum_fees = np.cumsum(actual[:len(regressor_bankroll)] * (-1.0) * fee)
        regressor_bankroll = regressor_bankroll + cum_fees

        regressor_line, = plt.plot(times[:-1], regressor_bankroll, label=('%d' % (fee * 100)) + '% Fee')
        regressor_lines.append(regressor_line)

    plt.legend(handles=regressor_lines, loc=2)
    plt.show()


def in_sample_period_length_opt(subreddit, pair):
    r2_values = []
    linear_backtest_results = []
    avg_backtest_results = []
    period_lengths = [10, 30, 1 * 60, 2 * 60, 3 * 60, 4 * 60, 5 * 60, 7*60, 10 * 60, 30 * 60, 60 * 60, 3 * 60 * 60, 6 * 60 * 60, 12 * 60 * 60, 24 * 60 * 60]
    num_periods_ls = []
    for p in period_lengths:
        _, in_sample_actual, in_sample_preds, _, _, _, times = full_ml(subreddit, pair, plot_sample_results=False,
                                                                       period_length=p, strategy=0)
        r2 = r_squared(np.diff(in_sample_actual), in_sample_preds[1:] - in_sample_actual[:-1])
        r2_values.append(r2)

        directions = ((in_sample_preds[1:] > in_sample_actual[:-1]) * 2 - 1)
        bankroll_total = sum(np.diff(in_sample_actual) * directions)
        linear_backtest_results.append(bankroll_total)

        mean = sum(in_sample_actual) * 1.0 / len(in_sample_actual)
        avg_bankroll_total = sum(np.diff(in_sample_actual) * (mean > in_sample_actual[:-1]))
        avg_backtest_results.append(avg_bankroll_total)

        num_periods_ls.append(len(in_sample_preds) - 1)

    # plt.figure('r2 values', figsize=figure_size)
    # ax = plt.subplot(111)
    # ax.set_xlabel('period length in seconds')
    # ax.set_ylabel('R^2 of variance returns')
    # plt.plot(period_lengths, r2_values)

    # plt.figure('backtest results values', figsize=figure_size)
    # ax = plt.subplot(111)
    # ax.set_xlabel('period length in seconds')
    # ax.set_ylabel('bankroll final value')
    # plt.plot(period_lengths, linear_backtest_results, c='g')
    # plt.plot(period_lengths, avg_backtest_results, c='c')

    # plt.figure('backtest results values over period length', figsize=figure_size)
    ax = plt.subplot(211)
    ax.set_xlabel('period length in seconds')
    ax.set_ylabel('bankroll final value')
    ax.set_title('backtest results values over period length')
    l_plot, = plt.plot(period_lengths, linear_backtest_results, c='g', label='Linear Regression Strategy')
    a_plot, = plt.plot(period_lengths, avg_backtest_results, c='c', label='Average Strategy')
    plt.legend(handles=[l_plot, a_plot])

    # plt.figure('average trade returns over period length', figsize=figure_size)
    ax = plt.subplot(212)
    ax.set_xlabel('period length in seconds')
    ax.set_ylabel('average period returns')
    ax.set_title('average trade returns over period length')
    l_plot, = plt.plot(period_lengths, [linear_backtest_results[i] / num_periods_ls[i] for i in range(len(period_lengths))], c='g', label='Linear Regression Strategy')
    a_plot, = plt.plot(period_lengths, [avg_backtest_results[i] / num_periods_ls[i] for i in range(len(period_lengths))], c='c', label='Average Strategy')
    plt.legend(handles=[l_plot, a_plot], loc=2)

    # plt.figure('average backtest returns per period over number of periods', figsize=figure_size)
    # ax = plt.subplot(111)
    # ax.set_xlabel('period length in seconds')
    # ax.set_ylabel('bankroll final value')
    # plt.plot(num_periods_ls, sum(linear_backtest_results) / len(linear_backtest_results), c='g')
    # plt.plot(num_periods_ls, sum(avg_backtest_results) / len(avg_backtest_results), c='c')

    plt.show()


def main():
    # plot_simple_comparison('monero', 'BTC_XMR', 3*60*60)
    #
    # subreddit = 'audiophile'
    subreddit = 'bitcoin'
    # subreddit = 'litecoin'
    # subreddit = 'monero'
    pair = 'USDT_BTC'
    # pair = 'BTC_LTC'
    # pair = 'BTC_XMR'
    # # simple plots
    # plot_simple_comparison(subreddit, pair, 60*60)
    # plot_posts_vs_returns_scatter(subreddit, pair, 3*60*60)
    # plot_posts_vs_abs_returns_scatter(subreddit, pair, 3*60*60)
    # plot_posts_vs_abs_returns_changes_scatter(subreddit, pair, 6*60*60)
    #
    # # plot across time periods
    # plt.figure()
    # plot_all_periods(subreddit, pair)
    # plt.show()
    #
    # # plot across features
    # plt.figure()
    # plot_variables_against_returns(subreddit, pair, 6*60*60)
    # plot_keywords_against_returns(subreddit, pair, 6*60*60)
    # plot_user_max_quantile(subreddit, pair, 3*60*60)

    # full_ml(plot_sample_results=True)
    backtest(subreddit, pair)
    # fee_comparison(subreddit, pair)


if __name__ == '__main__':
    main()
