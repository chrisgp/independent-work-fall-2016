### Chris Piller's Independent Work 2016 ###

This is most of my repository for Independent work this past semester. I've only included the cached dataframes in uncompressed form (and only the very relevant dataframes). I've taken the other data folders, removed less-important datasets (unused currencies, etc.) and compressed those folders into reddit_cache.tar.gz and good_data.tar.gz.

### Run it! ###

To get a taste of what my code does, run the following command (full_prediction.py is where most of the analysis logic is):


`python full_prediction.py`