import matplotlib.pyplot as plt
from reddit_comments import get_all_subreddit_comments, get_all_subreddit_submissions
from find_big_moves import plot_all_data
import matplotlib.dates as mdates
from datetime import datetime
import numpy as np


def day_of_dt(dt):
    return datetime(dt.year, dt.month, dt.day)


def datetimes_to_day_counts(timestamps):
    days = []
    counts = []
    current_day = day_of_dt(timestamps[0])
    count = 0
    for dt in timestamps:
        if dt != current_day:
            days.append(current_day)
            counts.append(count)
            count = 0
            current_day = day_of_dt(dt)
        count += 1
    days.append(current_day)
    counts.append(count)

    return days, counts


def main():
    # comments = get_all_subreddit_comments('monero')
    # print len(comments)

    # submissions = get_all_subreddit_submissions('monero')
    # timestamps = [datetime.utcfromtimestamp(int(s.created)) for s in submissions]
    # days, counts = datetimes_to_day_counts(timestamps)
    #
    # print zip(days, counts)[:10]
    #
    # ax = plt.subplot(211)
    # plt.plot(days, counts)
    # plt.subplot(2, 1, 2, sharex=ax)
    # plot_all_data('data_lousy/BTC_XMR')
    # plt.show()

    submissions = get_all_subreddit_submissions('monero')
    print len(submissions)
    timestamps = [int(s.created) for s in submissions]
    # days, counts = datetimes_to_day_counts(timestamps)

    # print zip(days, counts)[:10]

    ax = plt.subplot(211)
    plt.hist(timestamps, bins=range(1433164069, 1476450504, 3*86400))
    plt.subplot(2, 1, 2, sharex=ax)
    plot_all_data('good_data/BTC_XMR', use_datetimes=False)
    plt.show()

# def main():
#     submissions = get_all_subreddit_submissions('cryptocurrency')
#     print len(submissions)
#     timestamps = [int(s.created) for s in submissions if ('monero' in s.title.lower()) or ('XMR' in s.title.upper())]
#     # days, counts = datetimes_to_day_counts(timestamps)
#
#     # print zip(days, counts)[:10]
#
#     ax = plt.subplot(211)
#     plt.hist(timestamps, bins=range(1433164069, 1476450504, 3*86400))
#     plt.subplot(2, 1, 2, sharex=ax)
#     plot_all_data('data_lousy/BTC_XMR', use_datetimes=False)
#     plt.show()

# def main():
#     submissions = get_all_subreddit_submissions('cryptomarkets')
#     print len(submissions)
#     timestamps = [int(s.created) for s in submissions if ('monero' in s.title.lower()) or ('XMR' in s.title.upper())]
#     _, counts = datetimes_to_day_counts([datetime.utcfromtimestamp(t) for t in timestamps])
#
#     # print zip(days, counts)[:10]
#
#     ax = plt.subplot(211)
#     plt.hist(timestamps, bins=range(1433164069, 1476450504, 3*86400))
#     ax.set_xlim(-.1, max(counts) * 3)
#     plt.subplot(2, 1, 2, sharex=ax)
#     plot_all_data('data_lousy/BTC_XMR', use_datetimes=False)
#     plt.show()

#
# def main():
#     comments = get_all_subreddit_comments('monero')
#     print len(comments)
#     timestamps = [int(s.created) for s in comments]# if ('monero' in s.title.lower()) or ('XMR' in s.title.upper())]
#     _, counts = datetimes_to_day_counts([datetime.utcfromtimestamp(t) for t in timestamps])
#
#     # print zip(days, counts)[:10]
#
#     ax = plt.subplot(211)
#     plt.hist(timestamps, bins=range(1433164069, 1476450504, 3*86400))
#     # ax.set_xlim(-.1, max(counts) * 3)
#     plt.subplot(2, 1, 2, sharex=ax)
#     plot_all_data('data_lousy/BTC_XMR', use_datetimes=False)
#     plt.show()


if __name__ == '__main__':
    main()
