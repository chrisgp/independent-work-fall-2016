import reddit_comments
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from datetime import datetime
import numba


########################################################################################################################
# Get datasets from minimal input
########################################################################################################################


def get_trade_prices_and_timestamps(pair):
    f = open('good_data/' + pair + '_trades.csv')
    data = f.read().split('\n')
    f.close()
    trade_timestamps, prices = [], []
    for row in data:
        if len(row) < 2:
            continue
        cols = row.split(',')
        trade_timestamps.append(int(cols[0]))
        prices.append(float(cols[1]))

    return prices, trade_timestamps


def get_post_timestamps_and_num_comments(subreddit, submissions=True):
    if submissions:
        submissions = reddit_comments.get_all_subreddit_submissions(subreddit)
    else:
        submissions = reddit_comments.get_all_subreddit_comments(subreddit)
    post_timestamps = [int(s.created) for s in submissions]
    num_comments = [s.num_comments for s in submissions]
    return post_timestamps, num_comments


########################################################################################################################
# Put data into nice pandas df format
########################################################################################################################


def _round_timestamp(t, period_length):
    return t - (t % period_length)


@numba.jit(nopython=True)
def _timestamp_frequencies_np(timestamps, timestamp_posts, first_timestamp, period_length, num_periods):
    """
    Makes a numpy array with no labeling, that can be combined with the time indices in posts_per_period to
    make a pd Dataframe with the right counts
    """
    counts = np.zeros(num_periods)
    for i in range(len(timestamps)):
        t = timestamps[i]
        c = timestamp_posts[i]
        index = (t - (t % period_length) - first_timestamp) / period_length
        counts[index] += c
    return counts


@numba.jit(nopython=True)
def _trade_prices_np(timestamps, prices, first_timestamp, period_length, num_periods):
    """
    Makes a numpy array as in _timestamp_frequencies_np that represents a continuous time series, this time
    with prices. Assumes each timestamp corresponds to the price at the same index, and timestamps are sequential.
    """
    out = -1 * np.ones(num_periods)
    for i in range(len(timestamps)):
        t = timestamps[i]
        p = prices[i]
        index = (t - (t % period_length) - first_timestamp) / period_length
        out[index] = p
    for i in range(num_periods-1):
        if out[i+1] == -1.0:
            out[i+1] = out[i]
    return out


def timestamps_to_df(timestamps, period_length, posts_per_timestamps=None):
    """
    Converts a list of timestamps into an evenly-spaced pd DataFrame with counts of the number of timestamps per period
    :param timestamps:
    :param period_length: Length of each period in seconds
    :return: pd DataFrame of counts
    """
    timestamp_posts = posts_per_timestamps
    if posts_per_timestamps is None:
        timestamp_posts = [1 for _ in timestamps]

    datetime_indices = pd.date_range(start=datetime.fromtimestamp(_round_timestamp(min(timestamps), period_length)),
                                     end=datetime.fromtimestamp(_round_timestamp(max(timestamps), period_length)),
                                     freq=('%ds' % period_length))
    count_values = _timestamp_frequencies_np(np.array(timestamps), np.array(timestamp_posts),
                                             _round_timestamp(min(timestamps), period_length),
                                             period_length, len(datetime_indices))
    df = pd.DataFrame({'submissions': pd.Series(count_values, index=datetime_indices)})

    return df


def prices_to_df(timestamps, prices, period_length):
    datetime_indices = pd.date_range(start=datetime.fromtimestamp(_round_timestamp(min(timestamps), period_length)),
                                     end=datetime.fromtimestamp(_round_timestamp(max(timestamps), period_length)),
                                     freq=('%ds' % period_length))
    structured_prices = _trade_prices_np(np.array(timestamps), np.array(prices),
                                         _round_timestamp(min(timestamps), period_length),
                                         period_length, len(datetime_indices))
    df = pd.DataFrame({'prices': pd.Series(structured_prices, index=datetime_indices)})

    return df


########################################################################################################################
# Do something with data acquired
########################################################################################################################

def plot_simple_comparison(subreddit, pair, period_length, submissions=True):
    # get submission timestamps
    post_timestamps, num_comments = get_post_timestamps_and_num_comments(subreddit, submissions=submissions)
    posts_df = timestamps_to_df(post_timestamps, period_length, posts_per_timestamps=num_comments)
    print posts_df[:5]

    # get price data
    trade_prices, trade_timestamps = get_trade_prices_and_timestamps(pair)
    prices_df = prices_to_df(trade_timestamps, trade_prices, period_length)
    print prices_df[:5]

    ax1 = plt.subplot(211)
    ax1.set_ylabel('r/' + subreddit + ' submissions')
    # posts_df.submissions.plot() # should work, but kinda slow compared to the next line
    plt.plot(posts_df.index, posts_df.submissions)
    ax2 = plt.subplot(2, 1, 2, sharex=ax1)
    ax2.set_ylabel(pair + ' price')
    # prices_df.prices.plot() # should work, kinda slow compared to the next line
    plt.plot(prices_df.index, prices_df.prices)
    plt.show()


def plot_simple_comparison_monero():
    plot_simple_comparison('monero', 'BTC_XMR', 60 * 60, submissions=True)


def plot_posts_vs_returns_scatter(subreddit, pair, period_length, submissions=True):
    """
    Currently returns the scatter of abs of the returns over the number of submissions.
    :param subreddit:
    :param pair:
    :param period_length:
    :param submissions:
    :return:
    """
    # get submission timestamps
    post_timestamps, num_comments = get_post_timestamps_and_num_comments(subreddit, submissions=submissions)
    posts_df = timestamps_to_df(post_timestamps, period_length, posts_per_timestamps=num_comments)
    print posts_df[:5]

    # get price data
    trade_prices, trade_timestamps = get_trade_prices_and_timestamps(pair)
    prices_df = prices_to_df(trade_timestamps, trade_prices, period_length)
    print prices_df[:5]
    # combine and lag data one period
    returns = pd.Series(prices_df.diff().prices.values[2:], index=prices_df.index[:-2])

    # combine into one dataframe
    posts_returns_df = pd.DataFrame({'submissions': posts_df.submissions, 'returns': returns})
    posts_returns_df = posts_returns_df.dropna()

    # feature engineering
    # ############################################################
    # posts_returns_df['submissions'] = 2**posts_returns_df.submissions
    posts_returns_df['returns'] = posts_returns_df.returns.abs()
    # indices = posts_returns_df.submissions.values > 500
    # posts_returns_df = posts_returns_df[indices]
    # ############################################################

    ax = plt.subplot(111)
    ax.set_xlabel('number of submissions in a given %dmin period' % (period_length / 60))
    ax.set_ylabel('absolute value of price movement in the following %dmin period' % (period_length / 60))

    plt.scatter(posts_returns_df.submissions, posts_returns_df.returns, alpha=.1)

    # get line of best fit
    # from: stackoverflow.com/questions/19068862/how-to-overplot-a-line-on-a-scatter-plot-in-python/19069001#19069001
    m, b = np.polyfit(posts_returns_df.submissions.values, posts_returns_df.returns.values, 1)
    print 'line of best fit: y = %f * x + %f' % (m, b)
    print 'periods used: ', len(posts_returns_df.submissions)
    plt.plot(posts_returns_df.submissions.values, m * posts_returns_df.submissions.values + b, 'r')
    plt.show()

    return posts_returns_df


def plot_monero_posts_vs_returns_scatter_daily():
    plot_posts_vs_returns_scatter('monero', 'BTC_XMR', 24 * 60 * 60, submissions=True)


def plot_monero_posts_vs_returns_scatter_hourly():
    plot_posts_vs_returns_scatter('monero', 'BTC_XMR', 60 * 60, submissions=True)


########################################################################################################################
# Main
########################################################################################################################


def main():
    # plot_simple_comparison_monero()
    # plot_monero_posts_vs_returns_scatter_daily()
    # plot_monero_posts_vs_returns_scatter_hourly()
    # plot_simple_comparison('bitcoin', 'USDT_BTC', 60*60)
    plot_posts_vs_returns_scatter('bitcoin', 'USDT_BTC', 12*60*60)
    # plot_simple_comparison('monero', 'BTC_XMR', 60*60, submissions=True)
    # plot_simple_comparison('monero', 'BTC_XMR', 60*60, submissions=True)
    # plot_posts_vs_returns_scatter('monero', 'BTC_XMR', 12*60*60, submissions=True)
    # plot_posts_vs_returns_scatter('cryptocurrencies', 'BTC_XMR', 3*60*60)


if __name__ == '__main__':
    main()
