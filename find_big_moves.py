import matplotlib.pyplot as plt
from datetime import datetime


def plot_all_data(pair, use_datetimes=True):
    with open(pair + '_trades.csv') as f:
        data = f.read().split('\n')
        timestamps, prices = [], []
        for row in data:
            if len(row) < 2:
                continue
            cols = row.split(',')
            if use_datetimes:
                timestamps.append(datetime.utcfromtimestamp(int(cols[0])))
            else:
                timestamps.append(int(cols[0]))
            prices.append(float(cols[1]))

        plt.plot(timestamps, prices)


def has_big_move(pair):
    with open(pair + '_trades.csv') as f:
        data = f.read().split('\n')
        timestamps, prices = [], []
        for row in data:
            if len(row) < 2:
                continue
            cols = row.split(',')
            timestamps.append(int(cols[0]))
            prices.append(float(cols[1]))

        day_length = 24*60*60
        day_code = timestamps[0] / day_length
        day_min, day_max = prices[0], prices[0]
        for timestamp, price in zip(timestamps, prices):
            row_day_code = timestamp / day_length
            if row_day_code != day_code:
                # end day, check for big move
                if day_min > 0.0 and day_max / day_min >= 20.0:
                    return True
                day_code = row_day_code
                day_min, day_max = price, price
            if price < day_min:
                day_min = price
            if price > day_max:
                day_max = price
    return False


# Only makes sense for BTC pairs
def currently_has_value(pair):
    with open(pair + '_trades.csv') as f:
        data = f.read().split('\n')
        timestamps, prices = [], []
        for row in data:
            if len(row) < 2:
                continue
            cols = row.split(',')
            timestamps.append(int(cols[0]))
            prices.append(float(cols[1]))

        return prices[-1] >= .001


def main():
    f = open('pairs.txt')
    pairs = f.read().split('\n')
    f.close()

    for i, pair in enumerate(pairs):
        if has_big_move(pair):
            print pair, '%d/%d' % (i, len(pairs))
            plt.figure(pair)
            plot_all_data(pair)

    plt.show()


if __name__ == '__main__':
    main()
