import matplotlib.pyplot as plt

from find_big_moves import plot_all_data


def main():
    plot_all_data('BTC_LBC')
    plot_all_data('BTC_ETC')
    plt.show()


if __name__ == '__main__':
    main()
