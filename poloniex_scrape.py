import requests
import time
import json
from dateutil.parser import parse
import os

# https://poloniex.com/public?command=returnCurrencies

url_base = 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=%s&start=%d&end=%d'
spacing = 500000  # 60ish days


def get_url(pair, end_timestamp):
    # print url_base % (pair, end_timestamp - spacing, end_timestamp)
    return url_base % (pair, end_timestamp - spacing, end_timestamp)


def write_content(f, response, seen):
    if 'Invalid start time' in response:
        raise ValueError('Error when asking for more data')
    data = json.loads(response)
    trades_ls = []
    duplicate = False
    print len(data)
    if len(data) == 0:
        raise ValueError('Empty response from server')
    for entry in data:
        try:
            if entry['globalTradeID'] in seen:
                duplicate = True
                continue
        except:
            print data

        seen.add(entry['globalTradeID'])
        price_state = (int(time.mktime(parse(entry['date']).timetuple())), float(entry['rate']))
        trades_ls.append(price_state)
    # need to sort here? currently keeping server order
    for timestamp, price in trades_ls:
        f.write('%d,%f\n' % (timestamp, price))

    if len(trades_ls) > 0:
        print trades_ls[0], trades_ls[-1]
        return len(trades_ls), trades_ls[0][0], trades_ls[-1][0], duplicate
    else:
        print 0, 0
        return len(trades_ls), 0, 0, duplicate


def get_one_pair(pair):
    f = open(pair + '_trades.csv', 'w')
    end_timestamp = int(time.time())
    seen = set()
    while True:
        url = get_url(pair, end_timestamp)
        response = requests.get(url).content
        try:
            num_rows, first_time, last_time, duplicate = write_content(f, response, seen)
                # print 'num rows, last time = ', (num_rows, last_time)
        except ValueError:
            break
        if duplicate or num_rows == 50000:
            end_timestamp = last_time - 1
        else:
            end_timestamp -= spacing - 1
    f.close()


def main():
    # get all pairs
    response = requests.get('https://poloniex.com/public?command=returnOrderBook&currencyPair=all&depth=1').content
    pairs = json.loads(response).keys()
    f = open('pairs.txt', 'w')
    f.write('\n'.join(map(str, pairs)) + '\n')
    f.close()

    # write all pairs to their own files
    for pair in pairs:
        if os.path.isfile(pair + '_trades.csv'):
            print 'Skipping ' + pair
            continue
        print pair
        get_one_pair(pair)

    print 'finished'


if __name__ == '__main__':
    main()
