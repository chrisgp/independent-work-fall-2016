import matplotlib.pyplot as plt


def main():
    data = open('BTC_LBC_trades.csv').read().split('\n')
    timestamps, prices = [], []
    for row in data:
        if len(row) < 2:
            continue
        cols = row.split(',')
        timestamps.append(int(cols[0]))
        prices.append(float(cols[1]))
    plt.plot(timestamps, prices)
    plt.show()


if __name__ == '__main__':
    main()
